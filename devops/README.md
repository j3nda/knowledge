# Knowledge / DevOps

## hardware

### storage
- abbreviations &rarr; *(explain: what does they mean?)*
	- [ ] FDD &rarr; ______
	- [ ] HDD &rarr; ______
	- [ ] SSD &rarr; ______
	- [ ] MBR &rarr; ______
	- [ ] LVM &rarr; ______
	- [ ] LUKS &rarr; ______

- how its *<u>typical storage</u> organized? (eg: FDD, HDD)*
	- [ ] Q: what is inside? ______
	- [ ] Q: if i want to address specific part, how? ______

- <u>answers</u>
	- https://en.wikipedia.org/wiki/Hard_disk_drive
	- https://en.wikipedia.org/wiki/Master_boot_record
	- https://www.educative.io/answers/what-is-the-hard-disk-architecture-in-operating-systems

- file-systems &rarr; *(explain: what does they mean?)*
	- [ ] FAT &rarr; ______
		- [ ] FAT16 vs FAT32, Q: difference/limitations &rarr; ______
		- [ ] how its organized? &rarr; ______
	- [ ] NTFS &rarr; ______
	- [ ] ext2 or ext3 or ext4 &rarr; ______
		- [ ] Q: difference: ***ext2*** x ***ext3*** x ***ext4***?
			- [ ]  ***ext2*** vs ***ext3***? &rarr; ______
			- [ ]  ***ext3*** vs ***ext4***? &rarr; ______
		- [ ] Q: what is *inode* &rarr; ______
		- [ ] Q: limitations of *inode* &rarr; ______
	- [ ] brtfs &rarr; ______
	- [ ] xfs &rarr; ______
	- [ ] zfs &rarr; ______
	- [ ] squashfs &rarr; ______

	- <u>answers</u>
		- https://en.wikipedia.org/wiki/File_Allocation_Table
			- https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system
			- https://www.integralmemory.com/faq1/what-are-the-differences-between-fat16-fat32-and-exfat-file-systems/
		- https://en.wikipedia.org/wiki/NTFS
		- https://www.salvagedata.com/btrfs-zfs-xfs-ext4-how-are-they-different/

---

## networking

- [ ] TCP/IP &rarr; *(explain: what does they mean?)* &rarr; ______
	- [ ] TCP &rarr; ______
	- [ ] IP &rarr; ______
- [ ] UDP &rarr; *(explain: what does it mean?)* &rarr; ______
- [ ] OSI model, Q: ***what is it?*** &rarr; ______
	- [ ] Q: ***how many layers it has?*** &rarr; ______
	- [ ] Q: ***can u name them?*** &rarr; ______
- [ ] Q: difference: ***TCP/IP*** x ***UDP***? &rarr; ______
- <u>answers</u>
	- https://en.wikipedia.org/wiki/Internet_protocol_suite
		- https://en.wikipedia.org/wiki/Transmission_Control_Protocol
		- https://en.wikipedia.org/wiki/Internet_Protocol
	- https://en.wikipedia.org/wiki/User_Datagram_Protocol
	- https://en.wikipedia.org/wiki/OSI_model

### protocols

- common &rarr; *(filfull ports for protocols and/or describe abbreviations)*
	- [ ] ***FTP*** &rarr; port(s): ______
	- [ ] ***SSH*** &rarr; port(s): ______
	- [ ] ***HTTP*** &rarr; port(s): ______
	- [ ] ***HTTPS*** &rarr; port(s): ______
	- [ ] ***DNS*** &rarr; port(s): ______
	- [ ] ***DHCP*** &rarr; port(s): ______
	- [ ] ***NTP*** &rarr; port(s): ______
	- [ ] ***SNMP*** &rarr; port(s): ______
	- mail
		- [ ] ***SMTP***, port(s): ______
		- [ ] ***POP3***, port(s): ______
		- [ ] ***IMAP***, port(s): ______
- if i want to provide a <u>service on custom port</u>, Q: ***which port number(s) i can use?*** &rarr; ______
- Q: ***can i use port 0?*** &rarr; ______
- <u>answers</u>
	- https://en.wikipedia.org/wiki/Port_(computer_networking)
	- https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

---

## linux

- abbreviations &rarr; *(explain: what does they mean?)*
	- [ ] ASCII &rarr; ______
	- [ ] POSIX &rarr; ______
	- [ ] LSB &rarr; ______
	- [ ] LDP &rarr; ______
	- [ ] LILO or grub &rarr; ______
- Q: what is it? &rarr; *(explain: what does that mean?)*
	- [ ] /etc/hosts, Q: what is it? &rarr; ______
		- [ ] Q: how it looks like? &rarr; ______
	- [ ] process, Q: what is it? &rarr; ______
		- [ ] Q: how its identified? &rarr; ______
		- [ ] Q: how i can communicate with process? &rarr; ______
	- [ ] socket, Q: what is it? &rarr; ______
		- [ ] Q: how it looks like or how i can use it? &rarr; ______
	- [ ] service x daemon, Q: can u explain differencies? &rarr; ______
		- [ ] Q: if i want to write a *daemon*, what is typical life-cycle of it? &rarr; ______
	- [ ] firewall &rarr; ______
		- [ ] iptables / IN / FWD / OUT &rarr; ______
	- [ ] rpm / yum &rarr; ______
	- [ ] deb / dpkg &rarr; ______
- [ ] Q: what is linux-kernel? &rarr; ______
	- [ ] Q: what is actual/current version? &rarr; ______
	- [ ] Q: can u explain differencies between them? &rarr; ______
	- [ ] Q: how i will compile linux-kernel? &rarr; ______
- Q: can u describe/explain <u>typical linux file-structure</u>?
	- [ ] /boot &rarr; ______
	- [ ] /etc &rarr; ______
	- [ ] /home &rarr; ______
	- [ ] /proc &rarr; ______
	- [ ] /usr &rarr; ______
	- [ ] /var &rarr; ______
- [ ] Q: if i want to know, which linux-kernel is running, how i will do that? &rarr; ______
- [ ] Q: what is init-process? &rarr; ______
	- [ ] Q: which init-process providers do u know? &rarr; ______
- [ ] Q: what is run-level? &rarr; ______
	- [ ] Q: what is difference between run-level: 1 x 3? &rarr; ______

- <u>answers</u>
	- [ascii](https://en.wikipedia.org/wiki/ASCII),
	[posix](https://en.wikipedia.org/wiki/POSIX),
	[lilo](https://en.wikipedia.org/wiki/LILO_(bootloader)),
	[grub](https://en.wikipedia.org/wiki/GNU_GRUB),
	[process](https://www.tecmint.com/linux-process-management/),
	socket([network](https://en.wikipedia.org/wiki/Network_socket), [unix](https://en.wikipedia.org/wiki/Unix_domain_socket)), TBD:service,
	[daemon](https://en.wikipedia.org/wiki/Daemon_(computing)),
	[firewall](https://en.wikipedia.org/wiki/Firewall_(computing)),
	[linux-kernel](https://en.wikipedia.org/wiki/Linux_kernel),
	[kernel-version](https://phoenixnap.com/kb/check-linux-kernel-version),
	[rpm](https://en.wikipedia.org/wiki/RPM_Package_Manager),
	[deb](https://en.wikipedia.org/wiki/Deb_(file_format))
	- https://cs.wikipedia.org/wiki/Linux_Standard_Base
	- https://en.wikipedia.org/wiki/Linux_Documentation_Project
	- https://en.wikipedia.org/wiki/Hosts_(file)
	- https://en.wikipedia.org/wiki/Init
	- https://en.wikipedia.org/wiki/Runlevel

### distro

- [ ] Q: what is linux distribution? &rarr; ______
- Q: what are differencies between them? and which package system they use?
	- [ ] debian &rarr; ______
	- [ ] redhat &rarr; ______
	- [ ] slackware &rarr; ______
	- [ ] ubuntu &rarr; ______
	- [ ] fedora &rarr; ______
	- [ ] centOS &rarr; ______
	- [ ] gentoo &rarr; ______
	- [ ] linuxFromScratch &rarr; ______
	- [ ] Q: do u know another one? &rarr; ______
- [ ] Q: if i want to know, which linux-distribution is running, how i will do that? &rarr; ______
- <u>answers</u>
	- https://en.wikipedia.org/wiki/Linux_distribution
	- https://whatsmyos.com/
	- https://www.tecmint.com/find-linux-kernel-version-distribution-name-version-number/

### software

	dns
		bind
		unbound
		knot-dns
	mail
		sendmail
		qmail
		postfix
		courier-imap
	www
		apache
		nginx
	sql / not-sql
		mysql
		mariadb
		mongodb

### docker

	Dockerfile
	compose
	.env
