# Knowledge

## shlitz

- vs-code
	- https://marketplace.visualstudio.com/items?itemName=bierner.markdown-checkbox

- markdown
	- https://www.markdownguide.org/cheat-sheet/

---

## DevOps

- [README](devops/README.md)

---

## Contributors

- Jan Smid
